
import path from 'path'

export default class {
  static async run(srv_mg, config) {
    try {
      return config;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
