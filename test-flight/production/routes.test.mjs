
import path from 'path'

import FlyTest from "zunzun/flytest/index.mjs"
const test = FlyTest.test;
const test_async = FlyTest.test_async;
const fail = FlyTest.fail;


import WebContextManager from 'zunzun/webctx-mg/index.mjs'

export default class RoutesTest {
  constructor() {

  }

  static async exists(cfg) {
    try {
      return await test_async('Routes exist', async () => {
        try {
          cfg.webctx_mg.cwd = cfg.cwd;
          let route_mg = new WebContextManager({ cfg: cfg}, cfg.webctx_mg);
          for (let web_dir of route_mg.cfg.web_dirs) {
            const web_dir_path = path.resolve(route_mg.cfg.cwd, web_dir.dir);
            await route_mg.serve_context_group(web_dir.route, web_dir_path);
          }
          
          for (let ctx_group of route_mg.ctx_groups) {
            const ctxg_route = ctx_group.route_path.length > 1 ? ctx_group.route_path : '';
            for (let ctx of ctx_group.contexts) {
              if (!(ctx.route_path instanceof RegExp) && !(ctx.lctx && ctx.lctx.flyauth && ctx.lctx.flyauth.authorize)) {
                
                for (let pub_file of ctx.public_files) {
                  const test_url = encodeURI(`http://${cfg.host}:${cfg.port}${ctxg_route}${ctx.route_path}${pub_file.rel_path}`);
                  console.log("TEST URL:", test_url);
                  const resp = await fetch(test_url);
                  if (!resp.ok) {
                    return fail(
                      `Request to url ${test_url} failed`,
                      `Response: ${resp.status} ${resp.statusText}`
                    );
                  }
                }
              }
            }
          }
          return true;
        } catch (e) {
          console.error(e.stack);
          return false;
        }
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async correct_mime_types(cfg) {
    try {
      return await test_async('Correct mime types', async () => {
        try {
          cfg.webctx_mg.cwd = cfg.cwd;
          let route_mg = new WebContextManager({ cfg: cfg}, cfg.webctx_mg);
          for (let web_dir of route_mg.cfg.web_dirs) {
            const web_dir_path = path.resolve(route_mg.cfg.cwd, web_dir.dir);
            await route_mg.serve_context_group(web_dir.route, web_dir_path);
          }
          
          for (let ctx_group of route_mg.ctx_groups) {
            const ctxg_route = ctx_group.route_path.length > 1 ? ctx_group.route_path : '';
            for (let ctx of ctx_group.contexts) {
              if (!(ctx.route_path instanceof RegExp) && !(ctx.lctx && ctx.lctx.flyauth && ctx.lctx.flyauth.authorize)) {
                
                for (let pub_file of ctx.public_files) {
                  const test_url = `http://${cfg.host}:${cfg.port}${ctxg_route}${ctx.route_path}${pub_file.rel_path}`;
                  console.log("TEST URL:", test_url);
                  const resp = await fetch(test_url);
                  if (!resp.ok) {
                    return fail(
                      `Request to url ${test_url} failed!`,
                      `Response: ${resp.status} ${resp.statusText}.`
                    );
                  } else {
                    const header_mime = resp.headers.get('content-type');
                    if (header_mime != pub_file.type.mime) {
                      return fail(
                        `Request to url ${test_url} return invalid mime tipe!`,
                        `Expected: ${pub_file.type.mime}`,
                        `Got: ${header_mime}`
                      );
                    }
                  }
                }
              }
            }
          }
          return true;
        } catch (e) {
          console.error(e.stack);
          return false;
        }
      });
    } catch (e) {
      console.error(e.stack);
    }
  }


  static async redirects(cfg) {
    try {
      return await test_async('Redirects', async () => {
        try {
          cfg.webctx_mg.cwd = cfg.cwd;
          let route_mg = new WebContextManager({ cfg: cfg}, cfg.webctx_mg);
          for (let web_dir of route_mg.cfg.web_dirs) {
            const web_dir_path = path.resolve(route_mg.cfg.cwd, web_dir.dir);
            await route_mg.serve_context_group(web_dir.route, web_dir_path);
          }
          
          for (let ctx_group of route_mg.ctx_groups) {
            const ctxg_route = ctx_group.route_path.length > 1 ? ctx_group.route_path : '';
            for (let ctx of ctx_group.contexts) {
              if (!(ctx.route_path instanceof RegExp) && !(ctx.lctx && ctx.lctx.flyauth && ctx.lctx.flyauth.authorize)) {

                
                for (let pub_file of ctx.public_files) {
                  if (pub_file.name == "index.html") {
                    const test_urls = [
                      `http://${cfg.host}:${cfg.port}${ctxg_route}${ctx.route_path}`
                    ]
                    test_urls.push(test_urls[0].slice(0, -1));

                    let expected_redirect = `${test_urls[0]}index.html`;

                    if (ctxg_route.length == 0 && ctx.route_path.length == 1) {
                      expected_redirect = test_urls[0];
                      test_urls[0] = test_urls[0]+"index.html"
                      test_urls.splice(1, 1)
                    }

                    for (let test_url of test_urls) {
                      console.log("TEST URL:", test_url);
                      let resp = await fetch(test_url);
                      if (!resp.ok || resp.url !== expected_redirect) {
                        return fail(
                          `Request to url ${test_url} failed!`,
                          `Response: ${resp.status} ${resp.statusText}.`
                        );
                      }
                    }
                  }
                }
              }
            }
          }
          return true;
        } catch (e) {
          console.error(e.stack);
          return false;
        }
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async encoding(cfg) {
    try {
      return await test_async('Encoding', async () => {
        try {
          const accepted_encoding_variants = [
            'gzip, deflate, br',
            'gzip, deflate'
          ]

          cfg.webctx_mg.cwd = cfg.cwd;
          let route_mg = new WebContextManager({ cfg: cfg}, cfg.webctx_mg);
          for (let web_dir of route_mg.cfg.web_dirs) {
            const web_dir_path = path.resolve(route_mg.cfg.cwd, web_dir.dir);
            await route_mg.serve_context_group(web_dir.route, web_dir_path);
          }
          
          for (let ctx_group of route_mg.ctx_groups) {
            const ctxg_route = ctx_group.route_path.length > 1 ? ctx_group.route_path : '';
            for (let ctx of ctx_group.contexts) {
              if (!(ctx.route_path instanceof RegExp) && !(ctx.lctx && ctx.lctx.flyauth && ctx.lctx.flyauth.authorize)) {

                
                for (let pub_file of ctx.public_files) {
                  if (pub_file.type.encode) {
                    for (let aev of accepted_encoding_variants) {
                      const test_url = `http://${cfg.host}:${cfg.port}${ctxg_route}${ctx.route_path}${pub_file.rel_path}`;
                      console.log("TEST URL:", test_url, "Accept-Encoding", aev);
                      const resp = await fetch(test_url, {
                        headers: {
                          "Accept-Encoding": aev
                        }
                      });
                      if (!resp.ok) {
                        return fail(
                          `Request to url ${test_url} failed!`,
                          `Response: ${resp.status} ${resp.statusText}.`
                        );
                      } else {
                        const resp_enc = resp.headers.get("Content-Encoding");
                        if (aev.includes(`br`) && resp_enc !== 'br') {
                          return fail(
                            `Invalid response encoding for url ${test_url}`,
                            `Expected: br`,
                            `Got: ${resp_enc}`
                          );
                        } else if (!aev.includes(`br`) && resp_enc === 'br') {
                          return fail(
                            `Invalid response encoding for url ${test_url}`,
                            `Expected: gzip`,
                            `Got: ${resp_enc}`
                          );
                        } else if (!aev.includes(resp_enc)) {
                          return fail(
                            `Invalid response encoding for url ${test_url}`,
                            `Expected: ${aev}`,
                            `Got: ${resp_enc}`
                          );
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          return true;
        } catch (e) {
          console.error(e.stack);
          return false;
        }
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async caching(cfg) {
    try {
      return await test_async('Caching', async () => {
        try {
          cfg.webctx_mg.cwd = cfg.cwd;
          let route_mg = new WebContextManager({ cfg: cfg}, cfg.webctx_mg);
          for (let web_dir of route_mg.cfg.web_dirs) {
            const web_dir_path = path.resolve(route_mg.cfg.cwd, web_dir.dir);
            await route_mg.serve_context_group(web_dir.route, web_dir_path);
          }
          
          for (let ctx_group of route_mg.ctx_groups) {
            const ctxg_route = ctx_group.route_path.length > 1 ? ctx_group.route_path : '';
            for (let ctx of ctx_group.contexts) {
              if (!(ctx.route_path instanceof RegExp) && !(ctx.lctx && ctx.lctx.flyauth && ctx.lctx.flyauth.authorize)) {
                for (let pub_file of ctx.public_files) {
                  const cache_control = cfg.webctx_mg.dev_mode ? "" : "max-age=31536000";
                  const test_url = `http://${cfg.host}:${cfg.port}${ctxg_route}${ctx.route_path}${pub_file.rel_path}`;
                  console.log("TEST URL:", test_url, `Cache-Control: ${cache_control}`);
                  const resp = await fetch(test_url);
                  if (!resp.ok) {
                    return fail(
                      `Request to url ${test_url} failed!`,
                      `Response: ${resp.status} ${resp.statusText}.`
                    );
                  } else {
                    const resp_cc = resp.headers.get("Cache-Control");
                    if (resp_cc !== cache_control) {
                      return fail(
                        `Invalid response 'Cache-Control' header for url ${test_url}`,
                        `Expected: ${cache_control}`,
                        `Got: ${resp_cc}`
                      );
                    }
                  }
                }
              }
            }
          }
          return true;
        } catch (e) {
          console.error(e.stack);
          return false;
        }
      });
    } catch (e) {
      console.error(e.stack);
    }
  }
}
