
import FlyHTTP from 'zunzun/flyhttp/index.mjs'

import WebContextManager from 'zunzun/webctx-mg/index.mjs'

import path from 'path';

export default class FlyWeb {
  constructor(fhttp, sm, cfg) {
    this.http = fhttp
    this.gctx = {};
    this.sm = sm;
    this.cfg = cfg;

    this.http.get('/flyweb-language', async (req, res) => {
      try {
        if (cfg.available_languages.includes(req.params.tag)) {
          if (req.headers.referer) {

            const eqstr = req.headers.referer.includes("?") ?
              "?" + req.headers.referer.split("?")[1] : "";
            const dummy_url = new URL(`http://example.com${eqstr}`);

            dummy_url.searchParams.set('lang', req.params.tag);

            if (req.url.includes("?")) {
              const params = req.url.split('?')[1].split("&");
              for (let param of params) {
                if (param == "new_tab=true") {
                  dummy_url.searchParams.set('new_tab', true);
                  break;
                }
              }
            }

            const query_params = dummy_url.searchParams.toString();
            res.writeHead(302, {
              "Set-Cookie": [
                `language=${req.params.tag}; Path=/; SameSite=Strict; Secure; HttpOnly`
              ],
              'Location': `${req.headers.referer.split("?")[0]}?${query_params}`,
              ...fhttp.secure_headers
            });
            res.end("OK");
          } else {
            res.writeHead(200, {
              "Set-Cookie": [
                `language=${req.params.tag}; Path=/; SameSite=Strict; Secure; HttpOnly`
              ],
              ...fhttp.secure_headers
            });
            res.end("OK");
          }
        } else {
          throw new Error("Invalid language tag!");
        }
      } catch (e) {
        console.error(e.stack);
        res.writeHead(500, {
          "Content-Type": "text/plain",
          ...fhttp.secure_headers
        });
        res.end("Internal Server Error");
      }
    });
  }

  static async start(sm, cfg) {
    try {
      const fhttp = new FlyHTTP(cfg.host, cfg.port);
      const _this = new FlyWeb(fhttp, sm, cfg);

      cfg.webctx_mg.cwd = _this.app_path = sm.path;
      _this.route_mg = new WebContextManager(_this, cfg.webctx_mg);

      for (let web_dir of _this.route_mg.cfg.web_dirs) {
        const web_dir_path = path.resolve(_this.route_mg.cfg.cwd, web_dir.dir);
        const nctx_grp = await _this.route_mg.serve_context_group(web_dir.route, web_dir_path);
        await nctx_grp.serve();
      }

//      await _this.route_mg.serve();

/*
      fhttp.get("/", (req, res, next) => {
        req.some_val = "all set!";
        next();
      }, (req, res) => {
        console.log(req.some_val);
        res.writeHead(200, {'Content-Type': 'text/html'}); // http header
        res.write('<h1>Hello World!<h1>'); //write a response
        res.end(); //end the response
      });
*/
      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

}
